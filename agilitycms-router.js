const createApi = require('./helpers/api');
// const handleTheHandler = require('./helpers/handlers');

const createRouter = (config, options) => {
  // create agility API 
  const prodApi = createApi(config.guid, config.productionKey);
  const previewApi = createApi(config.guid, config.previewKey);
  return async (req, res, next) => {
    // to make sure it wont redirect to 404 when request favicon.ico
    if(req.url.toLowerCase() == '/favicon.ico') {
      next();
    }
    
    // check if there is a url param in the req agilitypreviewkey has length of over 10 characters
    const isPreview = false;
    const api = (isPreview) ? previewApi : prodApi;

    let sitemap = await api.getSitemap();

    console.log( req.url.toLowerCase() );

    let currentUrlSlug = req ? req.url.toLowerCase() : pathname;
        currentUrlSlug = (currentUrlSlug === '/' || currentUrlSlug === '') ? '/home' : currentUrlSlug;
    let thisSiteMapItem = sitemap[currentUrlSlug];

    // REDIRECT TO 404 - look into withRouter
    if (typeof thisSiteMapItem == 'undefined') {
      thisSiteMapItem = {
          redirect: {url: options.error404}
      }
    }
    
    // HANDLE REDIRECTS and Do not process if not needed
    if (!!thisSiteMapItem.redirect) {
        let redirectLocation = thisSiteMapItem.redirect.url.replace('~', '');
        res.redirect(redirectLocation);
        return;
    }
    
    
    var currentPage = await api.getPage(thisSiteMapItem.pageID);
    
    // get the handlers working
    Object.keys(currentPage.zones).map((zoneKey, zoneIndex) => {
      let zone = currentPage.zones[zoneKey];
      if (typeof options.zoneHandlers[zoneKey] == 'function') {
        zone = options.zoneHandlers[zoneKey](zone, api);
      }
      zone.map( agilityModule => {
        if (options.moduleHandlers[agilityModule.module]) {
          options.moduleHandlers[agilityModule.module](agilityModule, api);
        }
      });
    });

    // setup Pagedata
    res.pagedata = {
      currentPage,
      sitemap,
      thisSiteMapItem
    };


    // handlers
    if (typeof options.handler == 'function') {
      options.handler(req, res, next);
    } else if (options.handler == 'json') {
      // debug
      res.send(res.pagedata);
    } else if (options.handler == 'browserlog') {
      // console.log
      const html = `
      <html>
        <head>
          <title>Debug mode</title>
          <meta name="viewport" content="initial-scale=1.0, width=device-width" />
          <script>console.log(${JSON.stringify(res.pagedata)})</script>
        </head>
        <body>
          Please check your console for the page context.
          <br/>
          ${
            Object.keys(res.pagedata.sitemap).map((keyItem, i) => {
                return `<a href="${keyItem}">${res.pagedata.sitemap[keyItem].menuText}</a>`;
              })
          }
        </body>
      </html>`;
      res.send(html);
    }
  
    // fallback return
    next();
  }
}

module.exports = createRouter;