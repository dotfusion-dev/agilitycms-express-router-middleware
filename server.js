const express = require('express')
const app = express()
const port = 3000
const createRouter = require('./agilitycms-router');

const agilitycmsConfig = {guid: '3ce66757-e585-4c7d-8423-ecea3e05e42b', previewKey: 'xxx', productionKey: 'fetch.56f9d11a8d7af40da23f0005dc601249d3d654d8b9a91233c0a73d79b3b326f2'};
const agilityRouterOptions = {
  handler: 'browserlog', // can be a function that takes (req, res, next), or can be a 'json' -> raw json, or can be 'browserlog' -> console logs
  error500: '/errors/500',
  error404: '/errors/404',
  cache: true,
  zoneHandlers: {
    Masthead: (zone, api) => { 
      zone.push('xxx');
      return zone;
    }
  },
  moduleHandlers: {
    Heading: (module, api) => {
      module.newprop = 'xxxx';
      return module;
    }
  },
};

const agilityRouter = createRouter(
  agilitycmsConfig,
  agilityRouterOptions
);

app.get('/cache-clear', function(req, res) {
  res.send(JSON.stringify({status: 200, message: 'Cleared Cache success.'}));
});

app.get('*', agilityRouter);

app.listen(port, () => console.log(`
  Listening on http://localhost:${port}
`));