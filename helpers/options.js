const defaultOptions = {
  handler: 'browserlog', // can be a function that takes (req, res, next), or can be a 'json' -> raw json, or can be 'browserlog' -> console logs
  error500: '/errors/500',
  error404: '/errors/404',
  zoneHandlers: {
    Masthead: (zone, api) => { 
      zone.push('xxx');
      return zone;
    }
  },
  moduleHandlers: {
    Heading: (module, api) => {
      module.newprop = 'xxxx';
      return module;
    }
  },
}

module.exports = defaultOptions;